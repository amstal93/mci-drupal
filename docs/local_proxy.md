# Local proxy

There are many solutions for proxying containers to names on localhost, however solution by [Jason Wilder](http://jasonwilder.com) [nginx-proxy](https://github.com/jwilder/nginx-proxy) was relatively easy to implement and works smoothly.

## Starting docker proxy container

Creating the network and running one of the commands below ensures that even after computer (service) restart, it will restart proxy as well.

The best source of detailed information is in project [README.md](https://github.com/jwilder/nginx-proxy) file.

```sh
# First we need proxy net
docker network create proxy_net

# This is for SERVER
docker run \
  -d \
  --restart=unless-stopped \
  -p 80:80 \
  -p 443:443 \
  -v /data/proxy/certs:/etc/nginx/certs \
  -v /data/proxy/htpasswd:/etc/nginx/htpasswd \
  -v /data/proxy/conf.d:/etc/nginx/conf.d \
    -v /etc/nginx/vhost.d \
    -v /usr/share/nginx/html \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  -e DEFAULT_HOST=devbox21.com \
  --name d4d-proxy \
  --network=proxy_net \
  jwilder/nginx-proxy

# This is for LOCAL
docker run \
    -d \
    --restart=unless-stopped \
    -p 80:80 \
    -p 443:443 \
    -v ${HOME}/Sites/proxy/certs:/etc/nginx/certs \
    -v ${HOME}/Sites/proxy/htpasswd:/etc/nginx/htpasswd \
    -v ${HOME}/Sites/proxy/conf.d:/etc/nginx/conf.d \
      -v /etc/nginx/vhost.d \
      -v /usr/share/nginx/html \
    -v /var/run/docker.sock:/tmp/docker.sock:ro \
    -e DEFAULT_HOST=localhost \
    --name d4d-proxy \
    --network=proxy_net \
    jwilder/nginx-proxy

```

We are mounting three local directories to enable using **htpasswd** basic authentication and providing certificates in **certs**. Additional configurations are added in files in `conf.d` directory according to [proxy wide configurations](https://github.com/jwilder/nginx-proxy#proxy-wide).

We choose to use `proxy_net` as a network name where all containers will be joined and **proxy** will configure and expose them according to name given in environment variable of the container.

## Recognizing FQDN for containers

Add appropriately these lines to your `docker-compose.yml` file to have your container services available on domains

```
    # ...
    #   Add this to container
    #   and specify port if it is not standard 80
    #   it will be accessible on domain name without the need for port
    environment:
      VIRTUAL_HOST: my-project.dev.devbox21.com
      VIRTUAL_PORT: 8025
    networks:
      - proxy_net
      - default

# At the end, add network as external
networks:
  proxy_net:
    external: true

```

## Protecting sites with Basic HTTP Authentication

To protect a site with **Basic HTTP Authentication** you have to create a file in `htpasswd` with name same as domain, substitute **user** for project name and **password** for desired project name + xx

    printf "{{project}}:$(openssl passwd -crypt {{projectxx}})\n" | tee htpasswd/{{project}}.dev.devbox21.com
    printf "{{project}}:$(openssl passwd -crypt {{projectxx}})\n" | tee htpasswd/{{project}}.pma.devbox21.com
    printf "{{project}}:$(openssl passwd -crypt {{projectxx}})\n" | tee htpasswd/{{project}}.hog.devbox21.com

## Using HTTPS access

### Using self-signed certificate

To generate self-signed certificate you will have to issue the following command, change to ypur FQDN (fully qualified domain name). If you want to make certificate for all subdomains, use *.{{FQDN}} 

    openssl req -newkey rsa:2048 -nodes -keyout {{domain}}.key -x509 -days 1460 -out {{domain}}.crt

You will be asked to answer following questions according to what will be show in certificate (sample output with questions and answers):

    $ openssl req -newkey rsa:2048 -nodes -keyout example.com.key -x509 -days 1460 -out example.com.crt

    Generating a 2048 bit RSA private key
    .....+++
    ............................+++
    writing new private key to 'example.com.key'
    -----
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]:US 
    State or Province Name (full name) [Some-State]:New York
    Locality Name (eg, city) []:New York
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:My Awesome Company LLC
    Organizational Unit Name (eg, section) []:My department
    Common Name (e.g. server FQDN or YOUR name) []:*.example.com
    Email Address []:info@example.com

You should be in directory for certificates `/data/proxy/certs/` or move generated files there.

To check what is written in certificate, use

    openssl x509 -text -noout -in /data/proxy/certs/example.com.crt 

### Using valid certificates

In order to avoid self-signed certificate warnings, you need to use valid certificates you received from CA (certificate authority).

As with self generated, just move them to proper location, `/data/proxy/certs/`.

### Using LetsEncrypt service

We can also use this light service container that will provide us with valid certificate for our domains automatically, companion project to jwilder/nginx-proxy [JrCs Letsencrypt nginx proxy companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion) (you can read more details how it works there)

This command starts it in our environment

```
# This is for SERVER

docker run -d \
  --restart=unless-stopped \
  -v /data/proxy/certs:/etc/nginx/certs:rw \
  --volumes-from d4d-proxy \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  --name d4d-letsencrypt \
  --network=proxy_net \
  jrcs/letsencrypt-nginx-proxy-companion
  
# This is for LOCAL
docker run -d \
  --restart=unless-stopped \
  -v ${HOME}/Sites/proxy/certs:/etc/nginx/certs:rw \
  --volumes-from d4d-proxy \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  --name d4d-letsencrypt \
  --network=proxy_net \
  jrcs/letsencrypt-nginx-proxy-companion
  
```
